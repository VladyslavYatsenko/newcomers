FROM openjdk:8-jdk
ADD NEWCOMERS-src/target/newcomers.jar newcomers.jar
EXPOSE 8060 9001
ENTRYPOINT ["java","-jar","newcomers.jar"]