package com.company.newcomerssrc.services;

import com.company.newcomerssrc.entity.StudentEntity;

import java.util.List;

public interface StudentService {

    StudentEntity findById(Long id) throws Exception;

    void addStudent(StudentEntity studentEntity);

    void removeStudent(Long id);

    StudentEntity findOneByFirstNameAndLastName(String firstName, String lastName);

    List<StudentEntity> findAll();

    List<StudentEntity> findAllStudentWithMarkMoreThan(byte mark);

    List<StudentEntity> findAllStudentWithMarkLessThan(byte mark);

    List<StudentEntity> findAllByFirstName(String firstName);

    List<StudentEntity> findAllByLastName(String lastName);
}
