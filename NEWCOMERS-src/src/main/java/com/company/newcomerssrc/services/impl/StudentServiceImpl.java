package com.company.newcomerssrc.services.impl;

import com.company.newcomerssrc.entity.StudentEntity;
import com.company.newcomerssrc.repository.StudentRepository;
import com.company.newcomerssrc.services.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class StudentServiceImpl implements StudentService {

    private StudentRepository studentRepository;

    @Override
    public StudentEntity findById(Long id) {
        return studentRepository.findById(id).orElseThrow(RuntimeException::new);
    }

    @Override
    public void addStudent(StudentEntity studentEntity) {
        studentRepository.save(studentEntity);
    }

    @Override
    public void removeStudent(Long id) {
        studentRepository.deleteById(id);
    }

    @Override
    public StudentEntity findOneByFirstNameAndLastName(String firstName, String lastName) {
        return studentRepository.findByFirstNameAndLastName(firstName, lastName);
    }

    @Override
    public List<StudentEntity> findAll() {
        return studentRepository.findAll();
    }

    @Override
    public List<StudentEntity> findAllStudentWithMarkMoreThan(byte mark) {
        return studentRepository.findAllByMarkGreaterThanEqual(mark);
    }

    @Override
    public List<StudentEntity> findAllStudentWithMarkLessThan(byte mark) {
        return studentRepository.findAllByMarkLessThanEqual(mark);
    }

    @Override
    public List<StudentEntity> findAllByFirstName(String firstName) {
        return studentRepository.findByFirstName(firstName);
    }

    @Override
    public List<StudentEntity> findAllByLastName(String lastName) {
        return findAllByLastName(lastName);
    }

}
