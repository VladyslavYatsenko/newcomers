package com.company.newcomerssrc.controllers;

import com.company.newcomerssrc.entity.StudentEntity;
import com.company.newcomerssrc.services.StudentService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/students")
@AllArgsConstructor
@Slf4j
public class StudentController {
    private StudentService studentService;

    @GetMapping("/all")
    public ResponseEntity<List<StudentEntity>> getAllStudents() {
        return new ResponseEntity<>(studentService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/first_name/{first_name}")
    public ResponseEntity<List<StudentEntity>> getAllStudentsByFirstName(@PathVariable(name = "first_name") String firstName) {
        return new ResponseEntity<>(studentService.findAllByFirstName(firstName), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<StudentEntity> getStudentById(@PathVariable(name = "id") Long id) {
        try {
            return new ResponseEntity<>(studentService.findById(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/last_name/{last_name}")
    public ResponseEntity<List<StudentEntity>> getAllStudentsByLastName(@PathVariable(name = "last_name") String lastName) {
        return new ResponseEntity<>(studentService.findAllByLastName(lastName), HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<StudentEntity> addStudent(@RequestBody StudentEntity studentEntity) {
        studentService.addStudent(studentEntity);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
