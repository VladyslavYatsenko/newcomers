package com.company.newcomerssrc.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.security.PermitAll;

@Controller
@PermitAll
@Slf4j
public class MainController {

    @GetMapping("/")
    public String index() {
        log.info("Opened Index Page");
        return "index";
    }

}

