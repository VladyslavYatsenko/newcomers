package com.company.newcomerssrc.repository;

import com.company.newcomerssrc.entity.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface StudentRepository extends JpaRepository<StudentEntity, Long> {

    StudentEntity findByFirstNameAndLastName(String firstName, String lastName);

    List<StudentEntity> findByFirstName(String firstName);

    List<StudentEntity> findByLastName(String lastName);

    List<StudentEntity> findAllByMark(byte mark);

    List<StudentEntity> findAllByMarkLessThanEqual(byte mark);

    List<StudentEntity> findAllByMarkGreaterThanEqual(byte mark);

    Optional<StudentEntity> findById(Long id);
}
