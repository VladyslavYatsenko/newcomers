package com.company.newcomerssrc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewcomersSrcApplication {

    public static void main(String[] args) {
        SpringApplication.run(NewcomersSrcApplication.class, args);
    }

}
