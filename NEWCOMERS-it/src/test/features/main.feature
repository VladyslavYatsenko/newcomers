Feature: Students
  Scenario: I want to get student by id
    Given I have student id=1
    When I call student with this id=1 rest endpoint
    Then The response status should be 200 ok
    And response body should be id=1 "Ivan" "Petrov" with mark 4

