package test.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import test.model.StudentEntity;

@Repository
public interface StudentRepo extends JpaRepository<StudentEntity,Long> {
}
