package test.stepdefinitions;


import com.company.newcomerssrc.NewcomersSrcApplication;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.web.client.RestTemplate;
import test.model.StudentEntity;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = NewcomersSrcApplication.class, loader = SpringBootContextLoader.class)
@TestPropertySource(locations = "classpath:application-test.properties")
public class StepsDefinition {

    private RestTemplate restTemplate = new RestTemplate();
    private ResponseEntity<StudentEntity> response;
    private StudentEntity actual;

    @Given("^I have student id=(\\d+)$")
    public void iHaveStudentId(Long id) {
        actual = new StudentEntity(id, "Ivan", "Petrov", (byte) 4);
    }


    @When("^I call student with this id=(\\d+) rest endpoint$")
    public void iCallStudentWithThisIdRestEndpoint(int id) {
        response = restTemplate.getForEntity("http://localhost:8060/api/v1/students/" + id, StudentEntity.class);
    }

    @Then("^The response status should be (\\d+) ok$")
    public void theResponseStatusShouldBeOk(int status) {
        assertThat(response.getStatusCode().value()).isEqualTo(status);
    }


    @And("^response body should be id=(\\d+) \"([^\"]*)\" \"([^\"]*)\" with mark (\\d+)$")
    public void responseBodyShouldBeIdWithMark(Long id, String firstName, String lastName, int mark) {
        assertThat(actual).isEqualTo(new StudentEntity(id, firstName, lastName, mark));
    }

}
